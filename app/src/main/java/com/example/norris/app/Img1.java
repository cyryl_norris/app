package com.example.norris.app;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class Img1 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_img1);
    }

    public void back(View view) {
        finish();
    }
}
