package com.example.norris.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void viewImage1(View view) {
        Intent intent = new Intent(this, Img1.class);
        startActivity(intent);
    }

    public void viewImage2(View view) {
        Intent intent = new Intent(this, Img2.class);
        startActivity(intent);
    }

    public void viewImage3(View view) {
        Intent intent = new Intent(this, Img3.class);
        startActivity(intent);
    }
}
